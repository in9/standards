```EM REVISÃO```

#Migrations

###Regras gerais

Todas as alterações do banco de dados devem ser controladas e sendo assim devem ser usadas as seguintes metodologias para controlar as versões.

####Controladas pela aplicação

Quando for gerenciada pela aplicação devem ser referenciadas dentro da pastas ```database/migrations/```, onde terão todos arquivos em ordem por timestamp.
Sendo assim o nome dos arquivos será algo similar a ```{timetamp}_{name_of_migration}.php``` => ```20160103140600_create_table_users.php```

####Usando SQL

Quando não for possível usar um gerenciador de migrations na aplicação, devem ser criadas todas as SQL do banco de dados desde a criação das tabelas e todas as alterações a serem feitas e devem ser salvas dentro da pastas ```database/migrations/sql/```, onde terão todos arquivos em ordem por timestamp.
Sendo assim o nome dos arquivos será algo similar a ```{timetamp}_{name_of_migration}.php``` => ```20160103140600_create_table_users.sql```

###Ferramentas de Migrations

Seguem abaixo algumas ferramentas para gerenciamento de migrations:

* PHP
    * [Eloquent (ORM)](http://laravel.com/docs/5.1/migrations) - http://laravel.com/docs/5.1/migrations
    * [Phinx](https://github.com/robmorgan/phinx) - https://github.com/robmorgan/phinx
    * [Ruckusing](https://github.com/ruckus/ruckusing-migrations) - https://github.com/ruckus/ruckusing-migrations
    * [Ladder](https://bitbucket.org/drarok/ladder) - https://bitbucket.org/drarok/ladder