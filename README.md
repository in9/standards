```EM REVISÃO```

# Standards
Padrões de código usado nos projetos da [In9](in9web.com)

- [Backend](https://bitbucket.org/in9/standards/src/master/Backend.md?at=master)
- [Checklist de codificação e code review](https://bitbucket.org/in9/standards/src/master/Checklist.md?at=master)
- [Migrations](https://bitbucket.org/in9/standards/src/master/Migrations.md?at=master)
- [Padrões das URI's dos Serviços RPC](https://bitbucket.org/in9/standards/src/master/PadraoUriServicos.md?at=master)
